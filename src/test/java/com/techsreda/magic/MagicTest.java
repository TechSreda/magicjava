package com.techsreda.magic;

import com.anarsoft.vmlens.concurrent.junit.ThreadCount;
import com.techsreda.magic.dto.MasterOfPuppetsDto;
import com.techsreda.magic.entity.Magic;
import com.techsreda.magic.repository.MagicRepository;
import com.techsreda.magic.repository.MasterOfPuppetsRepository;
import com.techsreda.magic.service.MasterOfPuppetsService;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.assertj.core.api.Assertions.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@ContextConfiguration(initializers = {DBIntegrationTest.Initializer.class})
public class MagicTest {
    static final Logger LOGGER = LoggerFactory.getLogger(MagicTest.class);

    @Autowired
    private MasterOfPuppetsService masterOfPuppetsService;

    @Autowired
    private MasterOfPuppetsRepository masterOfPuppetsRepository;

    @Autowired
    private MagicRepository magicRepository;

    @Autowired
    private GenerateHelper generateHelper;


    private final static int THREAD_COUNT = 5;
    private AtomicReference<UUID> atomicUserId = new AtomicReference<>();

    @Before
    public void initialCount() {
        MasterOfPuppetsDto masterOfPuppetsDto = generateHelper.generateMOP();
        atomicUserId.set(masterOfPuppetsDto.getUserId());
    }

    @Test
    @ThreadCount(THREAD_COUNT)
    public void addOne() {
        LOGGER.info("start");
        masterOfPuppetsService.add(atomicUserId.get());
        LOGGER.info("stop");
    }

    @After
    public void testCount() {
        List<MasterOfPuppetsDto> masterOfPuppetsDtos = masterOfPuppetsService.get(atomicUserId.get());
        assertThat(masterOfPuppetsDtos.size()).isEqualTo(THREAD_COUNT);
    }

    @Test
    public void testNormalGenerateToDBAndBack() {

        MasterOfPuppetsDto masterOfPuppetsDto = generateHelper.generateMOP();

        Magic magic = magicRepository.findByMagicNumber(masterOfPuppetsDto.getMagics().get(0).getMagicNumber()).get();
        MasterOfPuppetsDto masterOfPuppetsDto1 = masterOfPuppetsService.getByMagicNumber(magic.getMagicNumber());

        assertThat(masterOfPuppetsDto).isEqualTo(masterOfPuppetsDto1);
    }

    @Test
    public void testThreads() throws InterruptedException {
        int POOL = 30;
        ExecutorService es = Executors.newFixedThreadPool(POOL);
        try {
            MasterOfPuppetsDto masterOfPuppetsDto = generateHelper.generateMOP();
            UUID userId = masterOfPuppetsDto.getUserId();

            for (int i = 0; i < POOL; i++) {
                es.execute(() -> masterOfPuppetsService.add(userId));
            }

            Thread.sleep(1000);

            es.shutdown();
            es.awaitTermination(1, TimeUnit.MINUTES);
        } finally {

        }
    }

}
