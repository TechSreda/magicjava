package com.techsreda.magic;

import com.techsreda.magic.dto.MasterOfPuppetsDto;
import com.techsreda.magic.service.MasterOfPuppetsService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class GenerateHelper {

    @Autowired
    private MasterOfPuppetsService masterOfPuppetsService;

    public MasterOfPuppetsDto generateMOP() {
        UUID userId = UUID.randomUUID();
        MasterOfPuppetsDto masterOfPuppetsDto = masterOfPuppetsService.add(userId);
        return masterOfPuppetsDto;
    }
}
