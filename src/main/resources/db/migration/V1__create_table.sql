CREATE TABLE master_of_puppets
(
    id      BIGSERIAL PRIMARY KEY,
    user_id UUID
);
CREATE INDEX master_of_puppets_user_id_idx ON master_of_puppets (user_id);

CREATE TABLE magic
(
    id                   BIGSERIAL PRIMARY KEY,
    magic_number         VARCHAR,
    master_of_puppets_id BIGINT REFERENCES master_of_puppets (id)
);
CREATE INDEX magic_master_of_puppets_id_idx ON magic (master_of_puppets_id);
CREATE UNIQUE INDEX number_idx ON magic (magic_number) WHERE magic_number IS NOT NULL;
