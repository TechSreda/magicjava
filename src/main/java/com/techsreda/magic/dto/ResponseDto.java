package com.techsreda.magic.dto;


import io.swagger.annotations.ApiModel;
import lombok.Value;

@ApiModel
@Value(staticConstructor = "of")
public class ResponseDto<T> {

    private T data;

    public static <T> ResponseDto<T> success(T data) {
        return ResponseDto.of(data);
    }

    public static ResponseDto<Void> success() {
        return ResponseDto.of(null);
    }

    public static <ET> ResponseDto<Void> failure() {
        return ResponseDto.of(null);
    }
}
