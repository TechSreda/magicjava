package com.techsreda.magic.dto;

import com.techsreda.magic.entity.Magic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MasterOfPuppetsDto {

    private Long id;
    private UUID userId;
    private List<Magic> magics = new ArrayList<>();
}
