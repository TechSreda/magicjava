package com.techsreda.magic.dto;

import com.techsreda.magic.audit.AuditMagicListener;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MagicDto {
    private Long id;
    private String magicNumber;
}
