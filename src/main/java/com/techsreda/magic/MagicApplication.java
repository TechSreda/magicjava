package com.techsreda.magic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MagicApplication {

    public static void main(String[] args) {
        SpringApplication.run(MagicApplication.class, args);
    }

}

