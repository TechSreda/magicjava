package com.techsreda.magic.controller;

import com.techsreda.magic.dto.MasterOfPuppetsDto;
import com.techsreda.magic.dto.ResponseDto;
import com.techsreda.magic.service.MasterOfPuppetsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController()
@RequestMapping("/api/v1/private/master")
@RequiredArgsConstructor
public class MasterController {

    private final MasterOfPuppetsService masterOfPuppetsService;

    @GetMapping(value = "{userId}")
    private ResponseDto<List<MasterOfPuppetsDto>> get(@PathVariable UUID userId) {
        return ResponseDto.success(masterOfPuppetsService.get(userId));
    }

    @PostMapping(value = "{userId}")
    private ResponseDto<MasterOfPuppetsDto> add(@PathVariable UUID userId) {
        return ResponseDto.success(masterOfPuppetsService.add(userId));
    }

    @GetMapping(value = "byMagicNumber/{magicNumber}")
    private ResponseDto<MasterOfPuppetsDto> get(@PathVariable String magicNumber) {
        return ResponseDto.success(masterOfPuppetsService.getByMagicNumber(magicNumber));
    }

}
