package com.techsreda.magic.repository;

import com.techsreda.magic.entity.Magic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;
import java.util.Optional;


@Repository
public interface MagicRepository extends JpaRepository<Magic, Long> {
    Optional<Magic> findByMagicNumber(String magicNumber);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<Magic> findById(Long id);
}
