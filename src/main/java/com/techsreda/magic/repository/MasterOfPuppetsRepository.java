package com.techsreda.magic.repository;

import com.techsreda.magic.entity.MasterOfPuppets;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;


@Repository
public interface MasterOfPuppetsRepository extends JpaRepository<MasterOfPuppets, Long> {
    Stream<MasterOfPuppets> findAllByUserId(UUID UserId);

    @Query("SELECT u FROM MasterOfPuppets u JOIN u.magics a WHERE a.magicNumber = :MagicNumber")
    Optional<MasterOfPuppets> findByMagicNumber(@Param("MagicNumber") String magicNumber);
}
