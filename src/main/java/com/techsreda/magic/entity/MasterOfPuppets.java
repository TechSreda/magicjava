package com.techsreda.magic.entity;

import com.techsreda.magic.audit.AuditMasterOfPuppetsListener;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditMasterOfPuppetsListener.class)
public class MasterOfPuppets {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private UUID userId;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "master_of_puppets_id")
    private List<Magic> magics = new ArrayList<>();
}
