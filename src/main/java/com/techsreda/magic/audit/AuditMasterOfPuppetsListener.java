package com.techsreda.magic.audit;

import com.techsreda.magic.entity.MasterOfPuppets;
import com.techsreda.magic.sqs.SenderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

@Component
@RequiredArgsConstructor
@Slf4j
public class AuditMasterOfPuppetsListener {

    private final SenderService senderService;

    @PostPersist
    @PostUpdate
    private void afterOperation(MasterOfPuppets masterOfPuppets) {
        log.info("afterOperation: masterOfPuppets:{}", masterOfPuppets);
        senderService.sendMasterOfPuppets(masterOfPuppets);
    }
}
