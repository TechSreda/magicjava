package com.techsreda.magic.audit;

import com.techsreda.magic.entity.Magic;
import com.techsreda.magic.sqs.SenderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

@Component
@RequiredArgsConstructor
@Slf4j
public class AuditMagicListener {

    private final SenderService senderService;

    @PostPersist
    @PostUpdate
    private void afterOperation(Magic magic) {
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
            @Override
            public void afterCommit() {
                super.afterCommit();
                log.info("beforeAnyOperation: account:{}", magic);
                senderService.sendMagic(magic);
            }
        });
    }
}
