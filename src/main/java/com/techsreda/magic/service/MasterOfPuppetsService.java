package com.techsreda.magic.service;

import com.techsreda.magic.dto.MasterOfPuppetsDto;
import com.techsreda.magic.entity.Magic;
import com.techsreda.magic.entity.MasterOfPuppets;
import com.techsreda.magic.mapper.MasterOfPuppetsMapper;
import com.techsreda.magic.repository.MasterOfPuppetsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class MasterOfPuppetsService {
    private final MasterOfPuppetsRepository masterOfPuppetsRepository;
    private final MasterOfPuppetsMapper masterOfPuppetsMapper;

    @Transactional
    public MasterOfPuppetsDto add(UUID userId) {
        log.debug("add: Starting with: userId: {}", userId);
        try {
            MasterOfPuppetsDto masterOfPuppetsDto = Optional.of(generateMasterOfPuppets(userId))
                    .map(masterOfPuppetsRepository::save)
                    .map(masterOfPuppetsMapper::map)
                    .get();

            log.info("add: Finished with: userId: {}", userId);
            return masterOfPuppetsDto;
        } catch (Exception oO) {
            log.error(String.format("add: Failed with: userId: %s", userId), oO);
            throw new RuntimeException(oO);
        }
    }

    @Transactional
    public List<MasterOfPuppetsDto> get(UUID userId) {
        log.debug("get: Starting with: userId: {}", userId);
        try {
            List<MasterOfPuppetsDto> masterOfPuppetsDtos = masterOfPuppetsRepository.findAllByUserId(userId)
                    .map(masterOfPuppetsMapper::map)
                    .collect(Collectors.toList());

            log.info("get: Finished with: userId: {}", userId);
            return masterOfPuppetsDtos;
        } catch (Exception oO) {
            log.error(String.format("get: Failed with: userId: %s", userId), oO);
            throw new RuntimeException(oO);
        }
    }

    @Transactional
    public MasterOfPuppetsDto getByMagicNumber(String magicNumber) {
        log.debug("getByMagicNumber: Starting with: magicNumber: {}", magicNumber);
        try {
            MasterOfPuppetsDto masterOfPuppetsDto = masterOfPuppetsRepository.findByMagicNumber(magicNumber)
                    .map(masterOfPuppetsMapper::map)
                    .get();

            log.info("getByMagicNumber: Finished with: magicNumber: {}", magicNumber);
            return masterOfPuppetsDto;
        } catch (Exception oO) {
            log.error(String.format("getByMagicNumber: Failed with: magicNumber: %s", magicNumber), oO);
            throw new RuntimeException(oO);
        }
    }

    private MasterOfPuppets generateMasterOfPuppets(UUID userId) {
        return new MasterOfPuppets(null,
                userId,
                List.of(
                        new Magic(null, UUID.randomUUID().toString())
                )
        );
    }

}
