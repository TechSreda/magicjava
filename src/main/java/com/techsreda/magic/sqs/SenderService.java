package com.techsreda.magic.sqs;

import com.techsreda.magic.entity.Magic;
import com.techsreda.magic.entity.MasterOfPuppets;
import com.techsreda.magic.repository.MasterOfPuppetsRepository;
import com.techsreda.magic.service.MasterOfPuppetsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
@Slf4j
public class SenderService {

    @Autowired
    @Lazy
    private MasterOfPuppetsService masterOfPuppetsService;

    @Autowired
    @Lazy
    private MasterOfPuppetsRepository masterOfPuppetsRepository;

    public void sendMagic(Magic magic) {
        log.debug("sendMagic: Started with magic:{}", magic);

        Optional<MasterOfPuppets> byMagicNumber = masterOfPuppetsRepository.findByMagicNumber(magic.getMagicNumber());

        log.info("sendMagic: Finished with magic:{}", magic);
    }

    public void sendMasterOfPuppets(MasterOfPuppets masterOfPuppets) {
        log.debug("sendMasterOfPuppets: Started with account:{}", masterOfPuppets);

        //Do something

        log.info("sendMasterOfPuppets: Finished with account:{}", masterOfPuppets);
    }

}
