package com.techsreda.magic.mapper;

import com.techsreda.magic.dto.MagicDto;
import com.techsreda.magic.entity.Magic;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MagicMapper {

    MagicDto map(Magic magic);
}
