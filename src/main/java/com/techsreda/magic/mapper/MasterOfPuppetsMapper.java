package com.techsreda.magic.mapper;

import com.techsreda.magic.dto.MasterOfPuppetsDto;
import com.techsreda.magic.entity.MasterOfPuppets;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MasterOfPuppetsMapper {

    MasterOfPuppetsDto map(MasterOfPuppets masterOfPuppets);
}
